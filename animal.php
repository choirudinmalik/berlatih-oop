<?php

class Animal {
  public $name;
  public $legs = 2;
  public $cold_blooded = false;
  public function __construct($name) {
    $this->name= $name;
  }
  public function get_name() {
    echo $this->name;
	echo "<br>";
  }
  
  public function get_legs() {
    echo $this->legs;
	echo "<br>";
  }
  
  public function get_cold_blooded() {
    if ($this->cold_blooded){
		echo "true";
	} else{
		echo "false";
	}
	echo "<br>";
  }
}

?>